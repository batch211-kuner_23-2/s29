// find letter "s" in first name and "d" in last name
db.users.find({$or: [{firstName: {$regex: 'S', $options: '$i'}}, {lastName: {$regex: 'D', $options: '$i'}}]}, {firstName: 1,
lastName: 1,_id: 0});


// find user who are HR dept. and age greater than or equal to 70
db.users.find({$and: [{department: "HR"}, {age: {$gte: 70}}]});

// find users with letter e in first name and has a age of less than or equal to 30
db.users.find(
    {$or: [
        {
            firstName: {$regex: 'E', $options: '$i'}
        }, 
        {
            age: {$lte: 30}
        }
        ]}
    );



