// MongoDB Query Operators

// Query Operator
/*
-definition
-importance
*/

// Types of Query Operator
/*
-greater than
-greater than or equal to
-less than
-less than or equal to
-not equal to
-in
*/


// 1. COMPARISON QUERY OPERATORS
/*
INCLUDES:
-greater than
-greater than or equal to
-less than
-less than or equal to
-not equal to
-in
*/


// GREATER THAN " $gt "
// finds documents that have fields number that are greater than a specified value.

db.users.find({age: {$gt: 76}});


// GREATER THAN or EQUAL TO" $gte "
// finds documents that have fields number that are greater than or equal to a specified value.

db.users.find({age: {$gte: 76}});


// LESS THAN " $lt "
// // finds documents that have fields number that are less than to a specified value.

db.users.find({age: {$lt: 65}});


// LESS THAN or EQUAL TO " $lte "
// finds documents that have fields number that are less than or equal to a specified value.

db.users.find({age: {$lte: 65}});


// NOT EQUAL TO " $ne "
// finds documents that have fields number that are not equal to a specified value.

db.users.find({age: {$ne: 65}});


// IN " $in "
// finds documents with specific match criteria on one field using different values.

db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({course: {$in: ["HTML", "React"]}});





// EVALUATION QUERY OPERATORS
// return data based on evaluations of either individual fields or the entire collections documents.


// REGEX (Regular Expressions) " $regex "
// they are called regular expressions because they are based on regular languages. And used for matching strings.


// Case Sensitive Query
/*
Syntax:
db.collectionName.find({field: {$regex: 'pattern'}});
*/

db.users.find({lastName: {$regex: 'A'}});


// Case Insensitive Query
/*
Syntax:
db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}});
*/

db.users.find({lastName: {$regex: 'A', $options: '$i'}});
// lowercase and uppercase are included


// Mini Activity1
db.users.find({firstName: {$regex: 'e'}});

db.users.find({firstName: {$regex: 'e', $options: '$i'}});


// LOGICAL QUERY OPERATORS

//  OR OPERATOR " $or "
// finds documents that match a single criteria from multiple provided search criteria.

db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

// AND OPERATOR " $and "

db.users.find({$and: [{age: {$ne:82}}, {age: {$ne: 76}}]});


// Mini Activity2

db.users.find({$and: [{firstName: {$regex: 'e', $options: '$i'}}, {age: {$lte: 30}}]});



// FIELD PROJECTION
/* 
by default, mongoDB returns the  whole document, especially when dealing with complex documents.
*/

// INCLUSION
// allow us to include/add specific fields only when retreiving documents. The value denoted is "1".
/*
Syntax:
db.collectionName.find({criteria}, {field: 1})
*/

db.users.find({firstName: "Jane"});

db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	"contact.phone": 1
}
);

// Exception to the INCLUSION rule: Suppressing the id field


db.users.find(
{
	firstName: "Jane"
},
{
	firstName: 1,
	lastName: 1,
	contact: 1,
	_id: 0
}
);


// SLICE OPERATOR " $slice "
// allows us to retrieve one element that matches the search criteria.

db.users.insert({
	nameArr: [
	{
		nameA: "Juan"
	},
	{
		nameB: "Tamad"
	}
	]
});

db.users.find({
	nameArr:
	{
		nameA: "Juan"
	}
});

// Let us use the SLICE OPERATOR
db.users.find(
	{"nameArr":
	{
		nameA: "Juan"
	},
	{
		nameArr:
		{$slice: 1}
	}
});

// Mini Activity3

db.users.find({firstName: {$regex: 's', $options: '$i'}});

db.users.find({},
{
	firstName: 1,
	lastName: 1,
	_id: 0
}
);